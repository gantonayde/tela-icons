**Tela Icon Theme**

A flat colorful Design icon theme for snaps.

**Attributions**:
This snap is packaged from vinceliuice's repository: https://github.com/vinceliuice/Tela-icon-theme

**Installation**
```
sudo snap install icon-theme-tela
```


To connect the icon theme to a snap, please run:

```
sudo snap connect [other snap]:icon-themes icon-theme-tela:icon-themes
```
